package db;


public interface RepositoryCatalog {

    public PersonRepository people();

    public RolePermissionRepository rolePerm();
    public UserRepository user();
    public AddressRepository address();
    public PermissionRepository permission();
    public RoleRepository role();
}
