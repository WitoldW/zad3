package db;

import domain.RolePermission;

import java.util.List;


public interface RolePermissionRepository extends Repository<RolePermission>  {
    public List<RolePermission> permissionWithRole(RolePermission role, PagingInfo page);
    public List<RolePermission> roleWithPermission(RolePermission role, PagingInfo page);

}
