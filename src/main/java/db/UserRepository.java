package db;

import domain.User;

import java.util.List;


public interface UserRepository extends Repository<User>  {
    public User withLogin(String login);
}
