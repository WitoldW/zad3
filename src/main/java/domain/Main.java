package domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;


import db.PagingInfo;
import db.PersonDbManager;
import db.RepositoryCatalog;
import db.catalogs.HsqlRepositoryCatalog;



public class Main {

    public static void main(String[] args) {

        String url = "jdbc:hsqldb:hsql://localhost/workdb";
        try(Connection connection = DriverManager.getConnection(url))
        {

            RepositoryCatalog catalogOf = new HsqlRepositoryCatalog(connection);

            catalogOf.people().withName("jan", new PagingInfo());


            PersonDbManager mgr = new PersonDbManager();

            List<Person> allPersons = mgr.getAll();

            for(Person p : allPersons){
                System.out.println(p.getId()+" "+p.getName()+" "+p.getLastName());
            }
            mgr.deleteById(3);

            Person toUpdate = new Person("Witold","W","48588798546");
            toUpdate.setId(22);

            mgr.update(toUpdate);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
