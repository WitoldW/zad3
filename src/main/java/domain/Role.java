package domain;



import java.util.ArrayList;
import java.util.List;

public class Role {

    private int id;
    private String role;
    private List<Permission> permissions = new ArrayList<Permission>();

    public Role(){

    }

    public Role(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Role(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public void addPermissions(Permission perm){
        permissions.add(perm);
    }

    @Override
    public String toString() {
        return "Role{" +
                "role='" + role + '\'' +
                ", permissions=" + permissions +
                '}';
    }
}
