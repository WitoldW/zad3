
import java.sql.Connection;

import java.sql.*;
import java.util.*;



import db.repos.*;
import domain.*;

public class Test {

    public static void main(String[] args) {

        String url = "jdbc:hsqldb:hsql://localhost/workdb";

        User one = new User("wit","Za1@wsx");
        Address two = new Address("City","Street","33-985");

        Permission execute = new Permission("execute");

        Permission read = new Permission("read");

        Role role = new Role("user");
        Role someRole = new Role("guest");
        List <Permission> l = new ArrayList<Permission>();
        l.add(execute);
        l.add(read);
        role.setPermissions(l);

        Person someone = new Person("Witek","Majkowski","4855877451");
        List <Address> l2 = new ArrayList<Address>();
        l2.add(two);
        someone.setAddress(l2);
        one.setPerson(someone);


        List <Role> l4 = new ArrayList<Role>();
        l4.add(role);
        l4.add(someRole);
        someone.setRoles(l4);

        try {

            Connection connection = DriverManager.getConnection(url);

            HsqlUserRepository userRepo = new HsqlUserRepository(connection);
            userRepo.add(one);
            one.setLogin("wicik");
            userRepo.modify(one);
            userRepo.remove(one);

            HsqlAddressRepository addressRepo = new HsqlAddressRepository(connection);
            addressRepo.add(two);
            two.setCity("Chroboszczyce");
            addressRepo.modify(two);
            addressRepo.remove(two);

            HsqlPermissionRepository permissionRepo = new HsqlPermissionRepository(connection);
            permissionRepo.add(execute);
            execute.setPermission("none");
            permissionRepo.modify(execute);
            permissionRepo.remove(execute);

            HsqlRoleRepository roleRepo = new HsqlRoleRepository(connection);
            roleRepo.add(role);
            role.setRole("admin");
            roleRepo.modify(role);
            roleRepo.remove(role);

            HsqlPersonRepository personRepo = new HsqlPersonRepository(connection);
            personRepo.add(someone);
            someone.setName("Atenk");
            someone.setLastName("Gajowski");
            personRepo.modify(someone);
            personRepo.remove(someone);


        } catch (SQLException e) {
            System.out.println("Coś poszło nie tak: "+ e);
        }

    }

}